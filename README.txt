-- SUMMARY --

Commerce save address to address on file displays checkbox for saving
the address on file in checkout process. So that saved address are visible
in address on file option next time.

-- REQUIREMENTS --

Commerce save address to address on file depends on commerce and
commerce_addressbook modules.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.

-- CONFIGURATION --

* Administration » Store » Configuration
  in CHECKOUT settings  configure the check out panes (billing information,
  shipping information ) where you need to enable save address options

  Enable the Address Book and save configuration.
